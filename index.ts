import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";

// Configuration

dotenv.config();

// Create Express App
const app: Express = express();
const port: String | Number = process.env.PORT || 8000;

// Define the first route of application
app.get("/", (req: Request, res: Response) => {
  // Send welcome message
  res.send("Welcome to App Express + Nodemon + TS + Mongoose + Swagger");
});

// Define the second route of application
app.get("/hello", (req: Request, res: Response) => {
  const username = req.query?.name ?? "Anónimo";
  // Send hello world
  res.send(`Hola ${username}`);
});

// Define the third route of application
app.get("/goodbye", (req: Request, res: Response) => {
  // Send goodbye world
  res.send('Goodbye, world');
});

// Execute app and listen in PORT
app.listen(port, () => {
  console.log(`EXPRESS SERVER: Running at http://localhost:${port}`);
});
